const log = console
const { BaseIterator, ComplexIterator } = require('./iterator.js')

const base = new BaseIterator()
const complex = new ComplexIterator()

// base
;(() => {
  log.info('BASE')
  let val
  while ((val = base.next()) !== null) {
    log.info(val)
  }
  log.info(val)
})()

// complex
;(() => {
  log.info('COMPLEX')
  let val
  while ((val = complex.next()) !== null) {
    log.info(val)
  }
  log.info(val)
})()
