const numbers = require('./numbers.js')

class BaseIterator {
  constructor () {
    this.generatedNumbers = numbers.generate()
    this.sortedNumbers = this.generatedNumbers.sort((a, b) => a - b)
  }
  next () {
    return this.sortedNumbers.length ? this.sortedNumbers.shift() : null
  }
}

class ComplexIterator extends BaseIterator {
  constructor () {
    super()
    this.base1 = new BaseIterator()
    this.base2 = new BaseIterator()
    this.base3 = new BaseIterator()
    this.base4 = new BaseIterator()

    console.log(
      this.base1.sortedNumbers,
      this.base2.sortedNumbers,
      this.base3.sortedNumbers,
      this.base4.sortedNumbers
    )

    this.sortedNumbers = [
      ...this.base1.sortedNumbers,
      ...this.base2.sortedNumbers,
      ...this.base3.sortedNumbers,
      ...this.base4.sortedNumbers
    ].sort((a, b) => a - b)
  }

  next () {
    return super.next()
  }
}

module.exports = {
  BaseIterator,
  ComplexIterator
}
