const generate = () => {
  const numbers = []
  for (let i = 0; i < 5; i++) {
    numbers.push(Math.floor(Math.random() * 20))
  }
  return numbers
}

module.exports = {
  generate
}
